This repository is container for SmidqeTweaks meant to be used as a userscript on berrytube.tv,
and will always be the latest uploaded version of it.

If you want to use this userscript install it from here:
https://smidqe.gitlab.io/smidqetweaks/smidqetweaks.user.js


If you want to create a script or a module to this tweaks, here's what the basic structure would contain

```javascript
function load() {
	const self = {
		meta: {
			name: '', //script name
			group: '', //script group (in settings)
			requires: [''] //array of modules that need to be loaded
		},

		//necessary if other modules/scripts depend on this module
		started: false,

		//menu items
		menu: [{
			id: '', //id that menu uses
			text: '', //title
			type: '', //button or menu
			sub: [] //sub items if type is menu, uses same {id, text, type}
		}],

		//window items
		windows: [{
			id: '',
			selectors: [''], //first value is default selector, 2 is maltweaks specific
			wrap: true,
			classes: [] //special classes to be applied to window
			titlebar: false //set false, if you don't want titlebar, otherwise you don't need it
		}],
		
		//toolbar items
		toolbar: [{
			id: '', //id
			element: '', //type of element
			text: '' //title
		}],


		//config is not really utilised by modules, mainly a script thing
		config: [{
			title: '', //title for a setting
			key: '' //key that settings use
		}],

		//notifications
		notifications: [''], //events that module/script outputs

		//what to do when module is enabled/disabled from settings
		enable: () => {},
		disable: () => {},

		//do something before module is enabled, variable initilization etc.
		init: () => {},
	}

	return self;
}

SmidqeTweaks.add(load())
```

Not every module need everything, enable/disable and meta object are necessary for every module.
Others are optional, and use them as you see fit.
