function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'chat',
			requires: ['windows']
		},
		started: false,
		modules: {},
		functions: [
			'addChatMsg', 'handleNumCount', 'addNewMailMessage'
		],
		container: null,
		berry: () => {

		},
		element: () => {
			return self.container;
		},
		user: (nick) => {
			let element = $(`#chatlist li[nick="${nick}"]`);
			let group = element.attr('class').split(' ')[1];

			return {
				nick: nick,
				group: group,
				lastPost: window.CHATLIST[nick] || 0,
				berry: element.hasClass('leader')
			};
		},
		users: () => {
			let users = {};

			for (let nick in window.CHATLIST)
				users[nick] = self.user(nick);

			return users;
		},
		usercount: () => {
			let users = self.users();
			let result = {
				all: window.CONNECTED,
				admin: 0,
				user: 0,
				anon: 0,
			};

			for (let user in users)
				result[users[user].group] += 1;

			return result;
		},
		posts: (data = {}) => {
			let posts = $('.msgwrap').toArray();
			let filter = '';

			switch (data.which || '') {
				case 'emotes': filter = '.berryemote'; break;
				case 'drinks': filter = '.drink'; break;
				case 'rcv': filter = '.rcv'; break;
				case 'flaunts': filter = '.nameflaunt'; break;
				case 'act': filter = '.act'; break;
				case 'poll': filter = '.poll'; break;

				default: break;
			}

			if (filter !== '')
				posts = posts.filter(elem => $(elem).find(filter).length > 0);

			if (data.nick)
				posts = posts.filter(elem => $(elem).attr('nick') === data.nick);

			return $(posts);
		},
		rcv: (nick) => {
			return self.posts({which: 'rcv', nick});
		},
		add: (nick, text, type, hideNick) => {
			let time = new Date();

			if (window.BerryTweaks)
				time = BerryTweaks.getServerTime();

			addChatMsg({
				msg: {
					nick,
					msg: text,
					metadata: {
						graymute: false,
						nameflaunt: false,
						flair: null,
						channel: 'main'
					},
					emote: type,
					timestamp: time,
				},

				ghost: false,
			}, "#chatbuffer");

			if (hideNick)
				self.posts({nick}).last().find('.nick').css('display', 'none');
		},
		display: (show) => {
			if (show)
				self.container.parent().removeClass('st-window-hidden');
			else
				self.container.parent().addClass('st-window-hidden');
		},
		adapt: () => {
			let reduce = 0;
			let window = self.modules.windows.get('userlist');

			if (!window)
				return;

			if (window.modular)
				return;

			$('.st-toolbar-row').toArray().forEach(elem => {
				let element = $(elem);

				if (!element.hasClass('st-window-hidden'))
					reduce += element.height();
			});

			self.container.add('#rcvOverlay, .st-window-users').css({
				top: `${reduce + 20}px`,
				'max-height': `calc(100% - ${reduce + 52}px`
			});
		},
		reset: () => {
			self.container.add('#rcvOverlay, .st-window-users').css({
				top: '',
				'max-height': '',
			});
		},
		highlight: (post) => {
			(post || self.last()).addClass('highlight');
		},
		last: (nick) => {
			return self.posts({nick}).last();
		},
		patch: (key, callback, after = true) => {
			if (self.functions.indexOf(key) === -1)
				return;

			SmidqeTweaks.patch({
				container: window,
				name: 'chat',
				func: key,
				after: after,
				callback: callback
			});
		},
		unpatch: (key, callback) => {
			SmidqeTweaks.unpatch({
				name: 'chat',
				func: key,
				callback: callback
			});
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
			self.started = true;
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.container = $('.chatbuffer');
			self.listen = [
				{id: SmidqeTweaks.constants.EVENT_WINDOW_MODULAR, callback: self.adapt},
				{id: SmidqeTweaks.constants.EVENT_MODULE_UNLOAD, callback: self.adapt}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
