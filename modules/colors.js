/*
	Could rework this
	listen to module load

	Colors: {
		id,
		src,
		dst,
		values: [],
	}
*/

function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'colors',
			requires: ['settings']
		},
		events: {
			EVENT_COLOR_QUEUE_ADD: "EVENT_COLOR_QUEUE_ADD",
			EVENT_COLOR_QUEUE_EMPTY: "EVENT_COLOR_QUEUE_EMPTY",
			EVENT_COLOR_UPDATE: "EVENT_COLOR_UPDATE"
		},
		attached: {},
		queue: [],
		modules: {},
		started: false,
		defaultSrc: '.chatbuffer',
		wutRefresher: null,
		copy: (src, dest, values) => {
			let res = values.map(attr => $(src).css(attr));

			$.each(res, (index, val) => {
				$(dest).css(values[index], val);
			});
		},
		get: (selector, values) => {
			let result = {};
			let element = $(selector);

			values.forEach((attr) => {
				result.attr = element.css(attr);
			});

			return result;
		},
		attach: (key, dest, data, update) => {
			self.attached[key] = {id: key, dest: dest, data: data};

			if (update)
				self.update(key);
		},
		unattach: (key) => {
			delete self.attached[key];
		},
		update: (key, ignoreTransition) => {
			let theme = key.indexOf('/') !== -1;
			let data = key && !theme ? [self.attached[key]] : Object.keys(self.attached).map(sub => self.attached[sub]);

			self.refreshWutColors();

			if (data.length === 0)
				return;

			self.queue = data.map((sub) => {
				return {id: sub.id, ignoreTransition: ignoreTransition, theme: theme};
			});

			SmidqeTweaks.notify(self.events.EVENT_COLOR_QUEUE_ADD, self.queue);
		},
		handle: (data) => {
			data.map(sub => {
				if (!self.attached[sub.id].data)
					return;

				let element = self.attached[sub.id];

				element.data.map(value => {
					let source = value.src || self.defaultSrc;
					let time = 0;

					if (sub.ignoreTransition)
						return self.copy(source, element.dest, value.css);

					//get the wait for transitions to happen if it exists (bloody maltweaks)
					time = $(source).css('transition-delay')
						.split(',')
						.reduce((sum, val) => sum + parseInt(val.replace(/\D/g, '')), 0);

					setTimeout(() => {
						self.copy(source, element.dest, value.css);

						if (self.queue.length === 0) {

							SmidqeTweaks.notify(self.events.EVENT_COLOR_QUEUE_EMPTY, {refreshWutColors: sub.theme});
							return;
						}

						self.queue = self.queue.filter(val => val.id === sub.id);
					}, time * 1000 + 500);
				});
			});
		},
		refreshWutColors: () => {
			if (!window.wutReloadUserColors)
				return;

			window.wutReloadUserColors();
		},
		enable: () => {
			SmidqeTweaks.patch({
				container: window,
				name: 'colors',
				func: 'setColorTheme',
				callback: self.update,
				after: true
			});

			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unpatch('colors', self.update);
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.started = true;

			self.listen = [
				{id: self.events.EVENT_COLOR_QUEUE_ADD, callback: self.handle},
				{id: self.events.EVENT_COLOR_QUEUE_EMPTY, callback: self.refreshWutColors}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
