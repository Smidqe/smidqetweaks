/*
	TODO:

	Configurations to menu:
		- hover
		- don't close on click
		- timed hiding of element (opacity 0)
			- configurable
*/

function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'menu',
			requires: ['windows', 'colors', 'settings'],
		},
		modules: {},
		events: {
			EVENT_MENU_DISPLAY: "EVENT_MENU_DISPLAY",
			EVENT_MENU_SUB_DISPLAY: "EVENT_MENU_SUB_DISPLAY",
			EVENT_MENU_ITEM_CREATE: "EVENT_MENU_ITEM_CREATE",
			EVENT_MENU_ITEM_DELETE: "EVENT_MENU_ITEM_DELETE",
			EVENT_MENU_ITEMS_CREATE: "EVENT_MENU_ITEMS_CREATE"
		},
		started: false,
		windows: [{
			classes: ['st-menu-new-container'],
			id: 'menu',
			titlebar: false,
		}],
		elements: {},
		container: null,
		callback: (id, event, callback) => {
			self.element(id).on(event, callback);
		},
		get: (id) => {
			return self.elements[id];
		},
		element: (id) => {
			return self.get(id).element;
		},
		group: (grp) => {
			let result = null;

			for (let id in self.elements) {
				if (self.elements[id].group === grp)
					result = self.elements[id];

				if (result)
					break;
			}

			return result;
		},
		display: (data) => {
			const notification = {
				event: null,
				data: null,
			};

			switch (data.dest) {
				case 'main': {
					self.modules.windows.display('menu', {show: data.show});

					notification.event = self.events.EVENT_MENU_DISPLAY;
					notification.data = {show: data.show};

					break;
				}
				case 'sub': {
					let target = $(data.target);
					let group = target.data('group');

					self.container
						.find('.st-menu-new-group')
						.attr('data-open', false);

					self.container
						.find(`.st-menu-new-group[data-group=${group}]`)
						.add(target)
						.add(self.container.find('#st-menu-new-subs'))
						.attr('data-open', data.show);

					notification.event = self.events.EVENT_MENU_SUB_DISPLAY;
					notification.data = {sub: data.dest, show: data.show};

					break;
				}
			}

			SmidqeTweaks.notify(notification.event, notification.data);
		},
		add: (data) => {
			if (data instanceof Array) {
				data.forEach(sub => self.add(sub));
				SmidqeTweaks.notify(self.events.EVENT_MENU_ITEMS_CREATE, data);
				return;
			}

			let main = self.container.find('#st-menu-new-main');
			let subs = self.container.find('#st-menu-new-subs');

			let base = $('<div>', {
				class: 'st-menu-new-item',
				id: `st-menu-new-item-${data.id}`,
				'data-group': data.group
			});

			self.elements[data.id] = {
				element: base,
				group: data.group,
				children: []
			};

			switch (data.format) {
				case 'menu': {
					base
						.attr('data-main', true)
						.attr('data-open', false)
						.append(
							$('<div>', {class: 'st-menu-new-item-data'}).append(
								$('<div>', {class: 'st-menu-new-dropdown-text'}).text(data.text || 'test'),
								$('<div>', {class: 'st-menu-new-dropdown-arrow'}).append(
									$('<i class="fas fa-sort-down"></i>')
								)
							)
						);

					$('<div>', {class: 'st-menu-new-group', 'data-group': data.group}).appendTo(subs);

					if (data.children)
						data.children.forEach(child => self.add({...child, group: child.group || data.group}));

					main.append(base);
					break;
				}
				case 'button': {
					base.append(
						$('<div>', {class: 'st-menu-new-button st-menu-new-item-data'})
							.text(data.text || "???")
					);

					if (data.group) {
						subs.find(`.st-menu-new-group[data-group=${data.group}]`).append(base);
						self.group(data.group).children.push(data.id);
					} else
						main.append(base);

					break;
				}
			}

			SmidqeTweaks.notify(self.events.EVENT_MENU_ITEM_CREATE, data.id);
		},
		check: (data) => {
			if (!data.menu)
				return;

			self.add(data.menu);
		},
		build: (data) => {
			if (self.container || data.id !== 'menu')
				return;

			let wrap = $('<div>', {id: 'st-menu-new-wrap', class: 'hidden'});

			self.container = data.element.wrap(wrap).parent();

			let menu = $('<div>', {id: 'st-menu-new-container'}).append(
				$('<div>', {id: 'st-menu-new-main', class: 'st-menu-new-items'}).append(
					$('<div>', {id: 'st-menu-new-info'}).text('SmidqeTweaks')
				),
				$('<div>', {id: 'st-menu-new-subs', class: 'st-menu-new-items', 'data-open': false})
			);

			self.container
				.append(
					$('<div>', {id: 'st-menu-new-toggle'})
						.append($('<i class="fas fa-bars"></i>'))
				)
				.on('mouseenter click', '#st-menu-new-toggle', (event) => {
					if (!self.modules.settings.get('menuHover', false) && event.type === 'mouseenter')
						return;

					let show = true;

					if (event.type === 'click')
						show = self.container.find('#st-window-container-menu').hasClass('st-window-hidden');

					self.display({show: show, dest: 'main'});
				})
				.on('click', '[data-main=true]', (event) => {
					self.display({dest: 'sub', target: event.currentTarget, show: $(event.currentTarget).attr('data-open') === 'false'});
				})
				.on('click', '.st-menu-new-button', () => {
					self.display({dest: 'sub', target: self.container.find('#st-menu-new-main [data-open=true]'), show: false});

					if (!self.modules.settings.get('menuNoClose', false))
						self.display({dest: 'main', show: false});
				})
				.on('mouseleave', () => {
					if (!self.modules.settings.get('menuHover', false))
						return;

					self.display({dest: 'main', show: false});
				})
				.find('#st-window-container-menu')
				.append(menu);

			self.started = true;
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.listen = [
				{id: "EVENT_WINDOW_CREATE", callback: self.build},
				{id: "EVENT_MODULE_LOAD", callback: self.check}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
