function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'playlist',
			requires: ['windows']
		},
		functions: [],
		modules: {},
		container: () => {
			return $('#playlist');
		},
		duration: (str) => {
			let values = str.split(":").reverse();
			let ms = 0;

			if (values.length > 3)
				return -1;

			$.each(values, (index) => {
				ms += Math.pow(60, index) * 1000 * parseInt(values[index]);
			});

			if (isNaN(ms))
				ms = -1;

			return ms;
		},
		exists: (method, value) => {
			return self.get(method, value).pos !== -1;
		},
		amount: (info) => {
			if (!info)
				return window.PLAYLIST.length;

			let result = {};

			self.toArray().forEach(element => {
				if (info !== 'all' && element.type !== info)
					return;

				if (!result[element.type])
					result[element.type] = 0;

				result[element.type] += 1;
			});

			return result;
		},
		diff: (prev, comp) => {
			let current = null;
			let search = self.get('title', prev);

			if (search.pos === -1)
				return 0;

			//if we haven't set the next video, compare to current active video
			current = self.get('title', comp || window.ACTIVE.videotitle);

			return search.pos - current.pos;
		},
		position: (method, value) => {
			return self.get(method || 'title', decodeURIComponent(value || window.ACTIVE.videotitle)).pos;
		},
		toArray: () => {
			let obj = window.PLAYLIST.first;
			let result = [];

			for (let i = 0; i < self.amount(); i += 1)
			{
				result.push({
					type: obj.videotype,
					obj: obj
				});

				obj = obj.next;
			}

			return result;
		},
		loaded: () => {
			return self.container().length > 0;
		},
		display: (show) => {
			if (!self.loaded())
				return;

			let container = self.container();

			if (show)
				container.removeClass('st-window-hidden');
			else
				container.addClass('st-window-hidden');
		},
		//TODO: Rework this
		get: (method, value, minHits = 1) => {
			let obj = window.PLAYLIST.first;
			let found = [];

			for (let i = 0; i < self.amount(); i += 1) {
				let result = false;

				switch (method) {
					case 'index': result = (value === i); break;
					case 'title-partial':
					case 'title': {
						if (method === 'title-partial')
							result = (decodeURIComponent(obj.videotitle).match(new RegExp(`^${value}$`, 'g')));

						//regex isn't forgiving with ()
						if (method === 'title')
							result = (decodeURIComponent(obj.videotitle) === value);

						break;
					}
				}

				if (result)
					found.push({value: obj, pos: i});

				if (minHits > 0 && found.length >= minHits)
					break;

				obj = obj.next;
			}

			if (found.length === 0)
				return {value: null, pos: -1};

			return minHits === 1 ? found[0] : found;
		},
		fixScroll: () => {
			window.smartRefreshScrollbar();
		},
		refresh: () => {
			scrollToPlEntry(Math.max($(".overview > ul > .active").index() - 2), 0);
			self.fixScroll();
		},
		delayedRefresh: () => {
			setTimeout(self.refresh, 1000);
		},
		delayedFixScroll: () => {
			setTimeout(self.fixScroll, 1000);
		},
		current: () => {
			return window.ACTIVE;
		},
		patch: (key, callback, after = true) => {
			if (self.functions.indexOf(key) === -1)
				return;

			SmidqeTweaks.patch({
				container: Object.getPrototypeOf(window.PLAYLIST),
				name: 'playlist',
				func: key,
				after: after,
				callback: callback
			});
		},
		unpatch: (key, callback) => {
			if (self.functions.indexOf(key) === -1)
				return;

			SmidqeTweaks.unpatch({
				name: 'playlist',
				func: key,
				callback: callback
			});
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.functions = Object.keys(Object.getPrototypeOf(window.PLAYLIST));
			self.started = true;
			self.listen = [
				{id: 'EVENT_WINDOW_MINIMIZE', callback: self.refresh},
				{id: 'EVENT_WINDOW_OPEN', callback: self.delayedRefresh},
				{id: 'EVENT_WINDOW_MODULAR', callback: self.delayedRefresh},
				{id: 'hbVideoDetail', callback: self.delayedFixScroll, socket: true}
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
