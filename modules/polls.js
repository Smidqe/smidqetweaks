/*
Option: {
	name: xxx
	votes: xxx
	ranked:
},
Poll: {
	name: xxx
	ranked: true/false
	active: true/false
	options: [
		Option...
	]
}
*/

function load() {
	const self = {
		meta: {
			name: 'polls',
			group: 'modules',
		},
		container: null,
		started: false,
		polls: () => {
			let result = [];
			let polls = $('.poll');

			if (polls.length === 0)
				return;

			result = polls.toArray().map(poll => {
				let element = $(poll);
				let options = [];
				let ranked = element.hasClass('ranked-poll');
				let info = {cont: 'tr', title: '.label', votes: '.btn'};
				let active = element.hasClass('active');

				if (ranked) {
					info = {
						cont: active ? '.ranked-poll__option' : '.ranked-poll__poll-option-result',
						title: active ? '.ranked-poll__option-text' : '.ranked-poll__poll-option-text',
						votes: '.ranked-poll__votes'
					};
				}

				options = element.find(info.cont).toArray().map((option, index) => {
					let elem = $(option);
					let obj = {
						text: elem.find(info.title).text(),
						votes: elem.find(info.votes).text(),
						ranked: ranked,
					};

					if (ranked) {
						let selected = elem.find('.is-selected:not(.is-abstain)');

						if (active)
							obj.voted = [selected.length > 0, selected.data('rank') + 1];
						else
							obj.result = [];
					}

					if (!ranked)
						obj.voted = [index, elem.find('.voted').length > 0];

					return obj;
				});

				return {
					title: element.find('.title').text(),
					ranked: ranked,
					active: active,
					options: options
				};
			});

			return result;
		},
		first: (active = true) => {
			for (let poll of self.polls()) {
				if (poll.active === active)
					return poll;
			}
		},
		hidden: (poll) => {
			return (poll || self.current()).find('.obscure').length > 0;
		},
		current: () => {
			let polls = self.polls();
			let result = polls.length > 0 ? polls[0] : {};

			return result;
		},
		get: (data) => {
			let result = [];

			self.polls().map(poll => {
				let match = false;

				switch (data.method) {
					case 'title':
					case 'title-partial': {
						match = poll.name.contains(data.value);
						break;
					}
					case 'option-count-exact': {
						match = poll.options.length === data.value;
						break;
					}
					case 'option-count-less': {
						match = poll.options.length <= data.value;
						break;
					}
					case 'option-count-more': {
						match = poll.options.length >= data.value;
						break;
					}
				}

				if (match)
					result.push(poll);
			});
		},
		active: (poll) => {
			return (poll || self.current()).hasClass('active');
		},
		voted: () => {
			return self.current().find('.voted').length > 0;
		},
		init: () => {
			self.container = $('#pollpane');
			self.started = true;
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
