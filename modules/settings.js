function load() {
	const self = {
		meta: {
			name: 'settings',
			group: 'modules'
		},
		events: {
			EVENT_SETTING_CHANGE: 'EVENT_SETTING_CHANGE',
		},
		container: null,
		started: false,
		settings: {},
		get: (key, def) => {
			return self.settings[key] || def;
		},
		set: (key, value) => {
			self.settings[key] = value;
			self.save();
		},
		save: () => {
			localStorage.SmidqeTweaks = JSON.stringify(
				Object.keys(self.settings).map((key) => ({key, value: self.settings[key]}))
			);
		},
		load: () => {
			JSON.parse(localStorage.SmidqeTweaks || '[]').forEach((setting) => {
				self.settings[setting.key] = setting.value;

				if (setting.value === true)
					self.script(setting.key, "ACTION_MODULE_LOAD");
			});
		},
		script: (key, action) => {
			if (SmidqeTweaks.names.scripts.indexOf(key) === -1)
				return;

			let script = SmidqeTweaks.modules[key];

			if (script && action === "ACTION_MODULE_LOAD")
				return;

			if (!script && action === "ACTION_MODULE_UNLOAD")
				return;

			SmidqeTweaks.module(action, {dir: 'scripts', name: key});
		},
		modify: (input, value) => {
			switch (input.attr('type')) {
				case 'checkbox': input.prop('checked', value); break;
			}

			self.container.off().on('change', 'input', self.onChange);
		},
		setting: (data, sub) => {
			const setting = $('<div>', {
				id: `st-setting-${data.key}`,
				class: 'st-setting-wrap'
			});

			const label = $('<label>', {
				text: data.title
			});

			const input = $('<input>', {
				type: data.element || 'checkbox',
				class: 'st-setting',
				'data-key': data.key,
			});

			self.modify(input, self.settings[data.key]);

			if (sub)
				setting.addClass('st-setting-sub');

			return setting.append(label, input);
		},
		add: (data) => {
			if (!data.config)
				return;

			let main = self.container.find(`#st-setting-${data.meta.name}`);

			data.config.forEach((setting) => {
				self.settings[setting.key] = self.get(setting.key, setting.value);
				main.after(
					self.setting(setting, true)
				);
			});

			self.container.off().on('change', 'input', self.onChange);
		},
		remove: (data) => {
			if (!data.config)
				return;

			data.config.forEach((setting) => self.container.find(`#st-setting-${setting.key}`).remove());
		},
		onChange: function() {
			let elem = $(this);
			let action = null;
			let value = null;

			//TODO: Add more
			switch (elem.attr('type')) {
				case 'checkbox': value = elem.prop('checked'); break;
			}

			if (SmidqeTweaks.names.scripts.indexOf(elem.data('key')) > -1)
				action = elem.prop('checked') ? "ACTION_MODULE_LOAD" : "ACTION_MODULE_UNLOAD";

			if (action)
				self.script(elem.data('key'), action);

			self.set(elem.data('key'), value);
			SmidqeTweaks.notify("EVENT_SETTING_CHANGE", {value});
		},
		build: () => {
			self.container.empty().append(
				$('<legend>', {text: 'SmidqeTweaks'})
			).append(Object.keys(SmidqeTweaks.groups).map((name) => {
				if (SmidqeTweaks.groups[name].length == 0)
					return;

				let group = $('<div>', {
					id: 'st-settings-group-' + name,
					class: 'st-settings-group'
				});

				group.append(
					$('<label>', {
						text: name[0].toUpperCase() + name.slice(1)
					})
				);

				if (name === 'others')
					group.append($('<label>', {id: 'st-settings-warn', text: 'Enable what you use'}));

				group.append(
					SmidqeTweaks.groups[name].map(
						script => self.setting({
							key: script,
							title: SmidqeTweaks.descriptions[script],
						})
					)
				);

				return group;
			}));
		},
		show: () => {
			$("#settingsGui > ul").append($('<li>').append(self.container));

			Object.keys(self.settings).forEach((setting) => {
				let input = self.container.find(`#st-setting-${setting}`).find('input');
				self.modify(input, self.get(setting, false));
			});

			self.container.off().on('change', 'input', self.onChange);
		},
		patch: (key, callback) => {
			SmidqeTweaks.patch({
				container: window,
				name: 'settings',
				func: key,
				after: true,
				callback: callback
			});
		},
		berrytweaks: key => {
			if (!window.BerryTweaks)
				return false;

			return BerryTweaks.loadSettings().enabled[key] || false;
		},
		init: () => {
			self.container = $('<fieldset>');

			SmidqeTweaks.listen({id: "EVENT_MODULE_LOAD", callback: self.add});
			SmidqeTweaks.listen({id: "EVENT_MODULE_UNLOAD", callback: self.remove});

			self.build();
			self.load();

			self.patch('showConfigMenu', self.show);
			self.started = true;
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
