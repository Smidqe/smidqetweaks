function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'stats',
			requires: ['windows', 'menu'],
		},
		menu: [
			{
				id: 'stats',
				title: 'Stats',
				type: 'button',
				element: 'button'
			}
		],
		started: false,
		container: null,
		modules: {},
		stats: {},
		add: (type, data) => {
			//don't add a duplicate value
			if (self.find(type, data.id))
				return;

			if (type === 'block')
			{
				let block = $('<div>', {class: 'st-stats-block', id: 'st-stats-block-' + data.id});
				let titlebar = $('<div>', {class: 'st-titlebar', id: 'st-stats-titlebar-' + data.id})
					.append(
						$('<div>', {
							class: 'st-stats-titlebar-button',
							'data-key': data.id
						}).on('click', self.modularize),
						$('<div>', {
							class: 'st-stats-titlebar-button',
							'data-key': data.id
						}).on('click', self.collapse)
					);

				block.append(titlebar);
				self.container.find('.st-stats-container').append(block);

				$.each(data.pairs || [], (key, pair) => {
					self.add('pair', pair);
				});
			}

			if (type === 'pair')
			{
				if (!data.block)
					return;

				let wrap = $('<div>', {class: 'st-stats-pair', id: 'st-stats-pair-' + data.id}).append(
					$('<span>', {class: 'st-stats-title'}).text(data.title),
					$('<span>', {class: 'st-stats-value'}).text(data.value ? data.value : 'Not set')
				);

				let block = self.find('block', data.block);

				if (block)
					block.append(wrap);
			}
		},
		find: (type, id) => {
			let select = null;

			if (type === 'block')
				select = $(self.container).find('#st-stats-block-' + id);

			if (type === 'pair')
				select = $(self.container).find('#st-stats-pair-' + id);

			return select[0] ? select : undefined;
		},
		remove: (name) => {
			if (!self.find('pair', name))
				return;

			self.container.find('#st-stats-pair-' + name).remove();
		},
		update: (dest, value) => {
			let pair = self.find('pair', dest);

			if (pair)
				$(pair).find('span:last-child').text(value);
		},
		show: () => {
			self.windows.show({name: 'stats', show: true});
		},
		init: () => {

		}
	};

	return self;
}

SmidqeTweaks.add(load());
