/*
Time = {
	time: ''
	format: ''
	suffix: ''
}
*/
function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'time'
		},
		formats: {
			FORMAT_12_HOURS: "FORMAT_12_HOURS",
			FORMAT_24_HOURS: "FORMAT_24_HOURS",
			FORMAT_MS: "FORMAT_MS",
		},
		suffix: {
			SUFFIX_PM: "SUFFIX_PM",
			SUFFIX_AM: "SUFFIX_AM",
		},
		get: (future) => {
			const time = future ? new Date(future) : new Date();

			let hours = time.getHours();
			let minutes = time.getMinutes();
			let seconds = time.getSeconds();

			let obj = {h: hours, m: minutes, s: seconds, format: '24h', suffix: null};

			$.each(obj, (key, val) => {
				if (key === 'format' || key === 'suffix') //don't do anything to format
					return;

				obj[key] = val < 10 ? "0" + val : val;
			});

			obj.suffix = obj.h > 12 ? 'PM' : 'AM';

			return obj;
		},
		diff: (old, current) => {
			return self.get(abs(self.convert(old, 'ms') - self.convert(current, 'ms')));
		},
		convert: (val, format) => {
			if (val.format === format)
				return;

			switch (format) {
				case 'ms': {
					if (val.format === '12h')
						val = self.convert(val, '24h');

					val.ms = parseInt(val.h) * (3600000) + parseInt(val.m) * 60000 + parseInt(val.s) * 1000;
					break;
				}
				case '12h': {
					if (val.format === 'ms')
						val = self.convert(self.get(val.ms), '12h');

					if (val.format === '24h') {
						val.suffix = val.h > 12 ? 'PM' : 'AM';
						val.format = '12h';
						val.h -= val.h > 12 ? 12 : 0;
					}

					break;
				}
				case '24h': {
					if (val.format === 'ms')
						val = self.get(val.ms);

					if (val.format === '12h')
						val.h += 12;

					val.format = '24h';
					val.suffix = '';

					break;
				}
			}

			return val;
		},
		init: () => {
			self.started = true;
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
