function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'toolbar',
		},
		events: {
			EVENT_TOOLBAR_ADD: "EVENT_TOOLBAR_ADD",
			EVENT_TOOLBAR_REMOVE: "EVENT_TOOLBAR_REMOVE",
			EVENT_TOOLBAR_MODIFY: "EVENT_TOOLBAR_MODIFY",
		},
		bar: null,
		callbacks: {},
		modules: {},
		started: false,
		refresh: () => {
			Object.keys(self.callbacks).map((sub) => {
				let element = self.get(sub);

				if (!element)
					return;

				element.off();

				self.callbacks[sub].map((e) => {
					element.on(e.event, e.callback);
				});
			});
		},
		create: (data) => {
			if (data instanceof Array)
				return data.map(sub => self.create(sub));

			let element = $(`<${data.element || 'div'}>`, {
				class: 'st-toolbar-element',
				id: 'st-toolbar-element-' + data.id}
			)
				.attr('title', data.tooltip || "")
				.text(data.text || "");

			self.bar.append(element);

			if (self.callbacks[data.id])
				self.callbacks[data.id].map(sub => element.on(sub.event, sub.callback));

			SmidqeTweaks.notify(self.events.EVENT_TOOLBAR_ADD, data.id);
		},
		callback: (id, event, callback) => {
			if (!self.callbacks[id])
				self.callbacks[id] = [];

			self.callbacks[id].push({
				event: event,
				callback: callback
			});

			self.refresh();
		},
		add: (data) => {
			if (!data.toolbar)
				return;

			self.create(data.toolbar);
		},
		get: (name) => {
			if (!name)
				return self.bar;

			return self.bar.find(`#st-toolbar-element-${name}`);
		},
		delete: (key) => {
			self.get(key).remove();
		},
		remove: (data) => {
			if (!data.toolbar)
				return;

			if (data instanceof Array)
				return data.map(sub => self.delete(sub.id));
		},
		hide: key => {
			self.get(key).css('display', 'none');
		},
		show: (key) => {
			self.get(key).css('display', 'block');
		},
		update: (key, value) => {
			self.bar.find('#st-toolbar-element-' + key).text(value);
		},
		enable: () => {
			if (window.TYPE >= 1) {
				$('#chattabs')
					.addClass('st-toolbar-row')
					.appendTo($('#chatControls'));
			}

			SmidqeTweaks.listen(self.listen);

			self.started = true;
		},
		disable: () => {
			self.bar.remove();
			SmidqeTweaks.unlisten(self.listen);

			if (window.TYPE >= 1) {
				$('#chattabs')
					.removeClass('st-toolbar-row')
					.prependTo($('#chatpane'));
			}
		},
		init: () => {
			self.bar = $("<div>", {id: "st-toolbar-wrap"});
			self.bar.prependTo("#chatControls");

			self.listen = [
				{id: SmidqeTweaks.constants.EVENT_MODULE_LOAD, callback: self.add},
				{id: SmidqeTweaks.constants.EVENT_MODULE_UNLOAD, callback: self.remove}
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
