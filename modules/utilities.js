function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'utilities'
		},
		started: true,
		enums: {
			ELEMENT_EDGE_TOP: 0,
			ELEMENT_EDGE_RIGHT: 1,
			ELEMENT_EDGE_BOTTOM: 2,
			ELEMENT_EDGE_LEFT: 3,
		},
		edge: (elem, mouse) => {
			let bounds = elem.getBoundingClientRect();
			let dists = [
				bounds.top - mouse.pageY,
				bounds.right - mouse.pageX,
				bounds.bottom - mouse.pageY,
				bounds.left - mouse.pageX
			];

			let result = {index: -1, value: -1};
			$.each(dists, (index, value) => {
				let absolute = Math.abs(value);

				if (result.index == -1 || absolute < result.value)
					result = {index: index, value: absolute};
			});

			return result;
		},
		linearCheck: (...args) => {
			let result = true;

			$.each(args, index => {
				if (!result)
					return;

				result = !!args[index];
			});

			return result;
		},
		waitFor: (data, time = 200) => {
			let interval = setInterval(() => {
				let clear = true;

				switch (data.type) {
					case 'element': {
						clear = data.checks
							.map(selector => $(selector))
							.filter(elem => elem.length === 0)
							.length === 0;

						break;
					}
					case 'variable': {
						let results = data.checks.map((sub) => {
							let vals = sub.split('.');
							let obj = window[vals[0]];

							if (!obj)
								return false;

							if (vals.length === 1 && obj)
								return true;

							for (let i = 1; i < vals.length; i += 1) {
								obj = obj[vals[i]];

								if (!obj)
									break;
							}

							if (obj)
								return true;
						});

						clear = results.filter(exists => !exists).length === 0;
						break;
					}
				}

				if (!clear)
					return;

				if (data.callback)
					data.callback();

				clearInterval(interval);
			}, time);
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
