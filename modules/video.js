/*
	Video related module
	Features:
		- Disable/re-enable video
		- Timestamp
		-

	Info: {
		title: '',
		timestamp: '',
		length: '',
		source: '',
	}

*/

function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'video',
			requires: ['playlist']
		},
		functions: {
			original: [
				'videoPlay', 'videoPlaying', 'videoPause',
				'videoPaused', 'videoSeekTo', 'videoLoadAtTime',
				'videoGetTime', 'videoGetState', 'removeCurrentPlayer',
				'videoSeeked', 'manageDrinks'
			],
			players: {},
			backedup: {},
		},
		started: false,
		events: {
			EVENT_VIDEO_REMOVE: "EVENT_VIDEO_REMOVE",
			EVENT_VIDEO_RESTORE: "EVENT_VIDEO_RESTORE"
		},
		modules: {},
		container: () => {
			return $('#videowrap');
		},
		replace: () => {
			return false;
		},
		remove: () => {
			if (window.videojs && videojs.getPlayers().vjs_player)
				videojs('vjs_player').dispose();

			if (PLAYER && PLAYER.PLAYER && PLAYER.PLAYER.getColor)//shitty way to check
				PLAYER.PLAYER.destroy();

			switch (window.VIDEO_TYPE) {
				case 'yt': {
					if (!PLAYERS.yt.PLAYER)
						break;

					PLAYERS.yt.PLAYER.destroy();
					delete PLAYERS.yt.PLAYER;

					break;
				}
			}

			self.display(false);

			self.functions.original.forEach(function(func) {
				window[func] = self.replace;
			});

			Object.keys(PLAYERS).forEach((key) => {
				self.functions.players[key] = window.PLAYERS[key].getTime;
				window.PLAYERS[key].getTime = self.replace;
			});

			SmidqeTweaks.notify(self.events.EVENT_VIDEO_REMOVE, {});
		},
		restore: () => {
			self.functions.original.forEach(function(func) {
				window[func] = self.functions.backedup[func];
			});

			Object.keys(self.functions.players).forEach(key => {
				window.PLAYERS[key].getTime = self.functions.players[key];
			});

			self.display(true);
			self.reload();

			SmidqeTweaks.notify(self.events.EVENT_VIDEO_RESTORE, {});
		},
		reload: () => {
			window.VIDEO_TYPE = null;
			window.PLAYER = false;

			window.socket.emit('refreshMyVideo');
		},
		display: (show) => {
			let element = self.container();

			if (show) {
				element.removeClass('st-window-hidden');
			} else {
				element.addClass('st-window-hidden');
			}
		},
		init: () => {
			self.started = true;

			self.functions.original.forEach(func => {
				self.functions.backedup[func] = window[func];
			});
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
