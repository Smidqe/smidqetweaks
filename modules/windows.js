function load() {
	const self = {
		meta: {
			group: 'modules',
			name: 'windows',
			requires: ['settings', 'utilities']
		},
		events: {
			EVENT_WINDOW_CREATE: "EVENT_WINDOW_CREATE",
			EVENT_WINDOW_REMOVE: "EVENT_WINDOW_REMOVE",
			EVENT_WINDOW_OPEN: "EVENT_WINDOW_OPEN",
			EVENT_WINDOW_CLOSE: "EVENT_WINDOW_CLOSE",
			EVENT_WINDOW_MODULAR: "EVENT_WINDOW_MODULAR",
			EVENT_WINDOW_MINIMIZE: "EVENT_WINDOW_MINIMIZE",
		},
		modules: {},
		windows: {}, //container to hold all windows (all are jquery selectors)
		listen: [],
		opened: [],
		started: false,
		chrome: false,
		element: (id) => {
			return self.windows[id].element;
		},
		get: (id) => {
			return self.windows[id];
		},
		exists: (id) => {
			return self.get(id) !== undefined;
		},
		modular: (id) => {
			return self.get(id).modular;
		},
		minimized: (id) => {
			return self.get(id).minimized;
		},
		open: (id) => {
			return self.get(id).open;
		},
		display: (id, data) => {
			let window = self.get(id);

			if (!window)
				return;

			if (data.show)
				window.element.removeClass('st-window-hidden');
			else
				window.element.addClass('st-window-hidden');

			if (data.show && data.modular)
				self.modularize(id, data.modular);

			//set window to open
			window.open = data.show;

			//remove from opened windows
			if (data.show && (self.opened.indexOf(id) === -1))
				self.opened.push(id);

			if (!data.show && !data.popped)
				self.opened.splice(self.opened.indexOf(id), 1);

			SmidqeTweaks.notify(self.events.EVENT_WINDOW_OPEN, {show: data.show, esc: data.popped});
		},
		modularize: (id, modular) => {
			if (modular === undefined)
				return;

			let window = self.get(id);

			if (modular)
				window.element.addClass('st-window-container-modular');
			else
				window.element.removeClass('st-window-container-modular');

			if (self.chrome && modular)
				window.element.addClass('st-patch-chrome');
			else
				window.element.removeClass('st-patch-chrome');

			if (modular) {
				window.element.draggable({
					handle: '.st-titlebar',
					start: () => {
						$('#videowrap').css('pointer-events', 'none');
					},
					stop: () => {
						$('#videowrap').css('pointer-events', 'all');
					},
					containment: 'window'
				});
			} else {
				window.element.css({
					'right': '',
					'left': '',
					'bottom': '',
					'top': '',
					'width': '',
					'height': '',
				});

				window.element.draggable('destroy');
			}

			window.modular = modular;

			SmidqeTweaks.notify(self.events.EVENT_WINDOW_MODULAR, {modular: modular});
		},
		minimize: (id, min) => {
			let window = self.get(id);
			let titlebar = window.element.find('.st-titlebar-minimize');

			if (!window)
				return;

			if (min) {
				window.element.addClass('st-window-minimized');
				titlebar.addClass('st-titlebar-minimize-rotated');
			} else {
				window.element.removeClass('st-window-minimized');
				titlebar.removeClass('st-titlebar-minimize-rotated');
			}

			window.minimized = min;
			SmidqeTweaks.notify(self.events.EVENT_WINDOW_MINIMIZE, {min: min});
		},
		titlebar: (id) => {
			return $('<div>', {
				class: 'st-titlebar'
			}).append(
				$('<div>', {
					class: 'st-titlebar-exit st-titlebar-button',
					'data-id': id,
				}).on('click', function() {
					if (self.modular(id))
						self.modularize(id, false);

					self.display($(this).data('id'), {show: false});
				})
					.append($('<i>', {
						class: 'far fa-window-close'
					})),
				$('<div>', {
					class: 'st-titlebar-modularize st-titlebar-button',
					'data-id': id,
				}).on('click', function() {
					self.modularize(id, !self.modular(id));
				}).append($('<i>', {
					class: 'far fa-clone'
				})),
				$('<div>', {
					class: 'st-titlebar-minimize st-titlebar-button',
					'data-id': id,
				}).on('click', () => {
					self.minimize(id, !self.get(id).minimized);
				}).append($('<i>', {
					class: 'fas fa-angle-down'
				}))
			);
		},
		remove: (name, removeContents) => {
			let element = self.element(name);

			SmidqeTweaks.notify(self.events.EVENT_WINDOW_REMOVE, name);

			element.find('.st-titlebar').empty().remove();

			if (removeContents)
				element.contents().remove();
			else
				element.contents().unwrap();

			element.remove();
			delete self.windows[name];
		},
		create: (data) => {
			if (data instanceof Array)
				return data.map(window => self.create(window));

			if (self.exists(data.id))
				return;

			let window = self.windows[data.id] = {
				id: data.id,
				element: null,
				modular: false,
				open: false,
				minimized: false
			};

			window.element = $('<div>', {
				id: 'st-window-container-' + data.id,
				class: `st-window-container st-window-hidden ${data.wrap ? 'st-window-wrap' : ''}`
			}).addClass((data.classes || []).join(" "));

			let selector = '';

			if (data.wrap)
				selector = data.selectors[self.modules.settings.get('maltweaks') && (data.selectors.length > 1) ? 1 : 0];

			self.modules.utilities.waitFor({
				type: 'element',
				checks: data.wrap ? [selector] : ['body'],
				callback: () => {
					let titlebar = null;

					if (data.titlebar !== false)
						titlebar = self.titlebar(data.id);

					if (data.wrap) {
						self.windows[data.id].element = $(selector)
							.wrapAll(window.element)
							.parent()
							.prepend(titlebar);
					} else {
						$('body').append(
							window.element.prepend(titlebar)
						);
					}

					SmidqeTweaks.notify(self.events.EVENT_WINDOW_CREATE, window);
				}
			});

			return window;
		},
		add: (data) => {
			if ((data.meta.name === 'windows') || !data.windows)
				return;

			data.windows.map(val => self.create(val));
		},
		delete: (data) => {
			if (!data.windows)
				return;

			data.windows.map(val => {
				self.remove(val.id, !val.wrap);
			});
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
			self.started = true;
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
			self.started = false;
		},
		init: () => {
			self.chrome = window.chrome !== undefined;
			self.listen = [
				{id: SmidqeTweaks.constants.EVENT_MODULE_LOAD, callback: self.add},
				{id: SmidqeTweaks.constants.EVENT_MODULE_UNLOAD, callback: self.delete}
			];

			window.addEventListener('keydown', (event) => {
				if (event.key != 'Escape')
					return;

				if (self.opened.length == 0)
					return;

				self.display(self.opened.pop(), {
					show: false,
					popped: true,
				});
			});
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
