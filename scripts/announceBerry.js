/*
const self = {
	info: {

	}
}

*/


function load() {
	const self = {
		meta: {
			name: 'announceBerry',
			group: 'scripts',
			requires: ['chat']
		},
		modules: {},
		config: [],
		chat: null,
		current: '',
		announce: (data) => {
			if (self.current === data.nick)
				return;

			self.current = data.nick;
			self.modules.chat.add('Server', data.nick ? `${data.nick} has been given berry` : 'Berry returned to server', 'rcv', true);
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.listen = [
				{id: 'leaderIs', callback: self.announce, socket: true}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
