function load() {
	const self = {
		meta: {
			name: 'berryControls',
			group: 'scripts',
			requires: ['settings', 'windows', 'playlist']
		},
		config: [],
		show: ['polls', 'playlist'],
		locations: {},
		elements: {},
		modules: {},
		modularize: (leader) => {
			if (!self.modules.settings.get('layout'))
				return;

			self.show.forEach(name => {
				self.modules.windows.display(name, {show: leader, modular: leader});
			});

			self.modules.playlist.refresh();
		},
		modular: (name) => {
			if (show.indexOf(name) === -1)
				return;

			self.modules.windows.modularize(name);
		},
		enable: () => {
			if (window.TYPE == 0)
				socket.on('setLeader', self.modularize);

			if (window.TYPE > 0)
				SmidqeTweaks.listen({id: 'EVENT_WINDOW_OPEN', callback: self.modular});
		},
		disable: () => {
			if (window.TYPE == 0)
				socket.removeListener('setLeader', self.modularize);

			if (window.TYPE > 0)
				SmidqeTweaks.unlisten({id: "EVENT_WINDOW_OPEN", callback: self.modular});
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
