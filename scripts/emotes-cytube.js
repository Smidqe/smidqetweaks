/* eslint-disable sort-vars */
/* eslint-disable consistent-this */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-negated-condition */

/*
 * Copyright (C) 2013 Marminator <cody_y@shaw.ca>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * COPYING for more details.
 */

Bem = typeof Bem === "undefined" ? {} : Bem;
Bem.jQuery = jQuery;
Bem.enabled = true;
Bem.community = "bt";

const shortRegex = /\\\\[aA-zZ]+/gi;
const berrytube_settings_schema = [
	{key: 'drunkMode', type: "bool", default: false},
	{key: 'effectTTL', type: "int", default: 20}
];

Bem.loggingIn = false;
Bem.refreshers = ['marminator', 'toastdeib', 'miggyb', 'jerick'];

Bem.berrySiteInit = function () {
	Bem.loadSettings(berrytube_settings_schema, function () {
		// Export it for backwards compat with BT squee inbox
		postEmoteEffects = Bem.postEmoteEffects;
		let invertScript;
		if (document.body.style.webkitFilter !== undefined) {
			invertScript = document.createElement('script');
			invertScript.type = 'text/javascript';
			invertScript.src = 'https://cdn.berrytube.tv/berrymotes/js/berrymotes.webkit.invert.js';
			document.body.appendChild(invertScript);
		} else {
			invertScript = document.createElement('script');
			invertScript.type = 'text/javascript';
			invertScript.src = 'https://cdn.berrytube.tv/berrymotes/js/berrymotes.invertfilter.js';
			document.body.appendChild(invertScript);
		}
		Bem.monkeyPatchChat();
		//        Bem.monkeyPatchPoll();
		//        Bem.monkeyPatchTabComplete();
		Bem.injectEmoteButton('.nav-collapsible ul.navbar-nav');
		$('form').submit(function () {
			Bem.loggingIn = true;
		});

		if (!Bem.enabled) {
			// do not emote existing chat messages if emotes are disabled
			return null;
		}

		function handleText(node) {
			Bem.applyEmotesToTextNode(node);
		}

		function walk(node) {
			// I stole this function from here:
			// http://is.gd/mwZp7E
			let child, next;
			switch (node.nodeType) {
				case 1: // Element
				case 9: // Document
				case 11: // Document fragment
					child = node.firstChild;
					while (child) {
						next = child.nextSibling;
						walk(child);
						child = next;
					}
					break;
				case 3: // Text node
					handleText(node);
					break;
			}
		}
		walk(document.body);
	});
};

// [name] is the name of the event "click", "mouseover", ..
// same as you'd pass it to bind()
// [fn] is the handler function
$.fn.bindFirst = function (name, fn) {
	// bind as you normally would
	// don't want to miss out on any jQuery magic
	this.on(name, fn);

	// Thanks to a comment by @Martin, adding support for
	// namespaced events too.
	this.each(function () {
		let handlers = $._data(this, 'events')[name.split('.')[0]];
		// take out the handler we just inserted from the end
		let handler = handlers.pop();
		// move it at the beginning
		handlers.splice(0, 0, handler);
	});
};



Bem.monkeyPatchChat = function () {
	let oldAddChatMsg = addChatMessage;
	addChatMessage = function (data, _to) {
		if (data.msg.match(shortRegex)) {
			data.msg = data.msg.replace(shortRegex, (match) => {
				return `[](/${match.substring(2)})`;
			});
		}

		//console.log(data);
		if (data.msg) {
			data.msg = data.msg.replace(/&#40;/, '(');
			data.msg = data.msg.replace(/&#41;/, ')');
		}
		let applyEmotes = Bem.enabled && data.msg.match(Bem.emoteRegex);

		if (applyEmotes) {
			data.msg = Bem.applyEmotesToStr(data.msg);
		}
		Bem.effectStack = $.grep(Bem.effectStack, function (effectEmote, i) {
			effectEmote.ttl -= 1;
			if (effectEmote.ttl >= 0) {
				return true; // keep the element in the array
			}
            
			effectEmote.$emote.css("animation", "none");
			return false;
            
		});
		oldAddChatMsg.apply(this, arguments);
		if (applyEmotes) {
			let chatMessage = $('#messagebuffer').children(':last-child');
			Bem.postEmoteEffects(chatMessage, false, Bem.effectTTL, data.username);
		}
	};
};

Bem.siteSettings = function (configOps) {
	//----------------------------------------
	//----------------------------------------
	row = $('<div/>').appendTo(configOps);
	$('<span/>').text("Max chat lines to keep effects running on (saves CPU):").appendTo(row);
	let chatTTL = $('<input/>').attr('type', 'text').val(Bem.effectTTL).addClass("small").appendTo(row);
	chatTTL.css('text-align', 'center');
	chatTTL.css('width', '30px');
	chatTTL.keyup(function () {
		Bem.effectTTL = chatTTL.val();
		Bem.settings.set('effectTTL', chatTTL.val());
	});
	//----------------------------------------
	row = $('<div/>').appendTo(configOps);
	let refresh = $('<button>Refresh Data</button>').appendTo(row);
	refresh.click(function () {
		Bem.emoteRefresh(false);
	});
};

Bem.settings = {
	get: function (key, callback) {
		let val = localStorage.getItem(key);
		callback(val);
	},
	set: function (key, val, callback) {
		localStorage.setItem(key, val);
		if (callback) callback();
	}
};

Bem.settings.set('siteWhitelist', ['berrytube.tv', 'www.berrytube.tv', 'cytu.be']);


//Bem.emoteRefresh = function() {
//    $.getScript('http://backstage.berrytube.tv/marminator/berrymotes_data.js', function () {
//        Bem.buildEmoteMap();
//    });
//};

Bem.emoteRefresh = function (cache) {
	cache = cache !== false;
	$.ajax({
		cache: cache,
		url: '//cdn.berrytube.tv/berrymotes/data/berrymotes_json_data.v2.json',
		dataType: 'json',
		success: function (data) {
			Bem.emotes = data;
			Bem.buildEmoteMap();
		}
	});
};

Bem.settings.get('apngSupported', function (apngSupported) {
	Bem.apngSupported = true;
});

let script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://cdn.berrytube.tv/berrymotes/js/berrymotes.core.js';
document.body.appendChild(script);
Function.prototype.debounce = function (milliseconds, context) {
	let baseFunction = this,
		timer = null,
		wait = milliseconds;

	return function () {
		let self = context || this,
			args = arguments;

		function complete() {
			baseFunction.apply(self, args);
			timer = null;
		}

		if (timer) {
			clearTimeout(timer);
		}

		timer = setTimeout(complete, wait);
	};
};
Function.prototype.throttle = function (milliseconds, context) {
	let baseFunction = this,
		lastEventTimestamp = null,
		limit = milliseconds;

	return function () {
		let self = context || this,
			args = arguments,
			now = Date.now();

		if (!lastEventTimestamp || now - lastEventTimestamp >= limit) {
			lastEventTimestamp = now;
			baseFunction.apply(self, args);
		}
	};
};

$.getScript('https://q-z.xyz/cystuff/malcytweak2.js');
