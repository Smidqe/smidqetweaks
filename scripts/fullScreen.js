/*
	TODO:
	- Clean
	- Hide chat properly
	- Add a button to show the chat/menu

*/

function load() {
	const self = {
		meta: {
			name: 'fullScreen',
			group: 'scripts',
			requires: ['chat', 'video', 'menu']
		},
		config: [],
		menu: [{
			id: 'hideControls',
			format: 'menu',
			text: 'Fullscreen',
			group: 'fullscreen',
			children: [
				{id: 'hideVideo', format: 'button', text: 'Hide video'},
				{id: 'hideChat', format: 'button', text: 'Hide chat'}
			]
		}],
		modules: {},
		hidden: {
			HIDDEN_VIDEO: false,
			HIDDEN_CHAT: false,
		},
		started: false,
		display: (data) => {
			console.log(data);

			switch (data.which) {
				case 'video': {
					self.hidden.HIDDEN_VIDEO = data.hide;

					if (data.hide) {
						self.modules.video.remove();
					} else
						self.modules.video.restore();

					break;
				}
				case 'chat': {
					self.hidden.HIDDEN_CHAT = data.hide;
					self.modules.chat.display(!data.hide);
					break;
				}
			}

			let element = data.which === 'video' ? self.modules.chat.element().parent() : self.modules.video.container();

			if (data.hide)
				element.addClass('st-window-fullscreen');
			else
				element.removeClass('st-window-fullscreen');
		},
		text: (id, text) => {
			self.modules.menu.element(id).find('.st-menu-new-item-data').text(text);
		},
		chat: () => {
			self.text('hideChat', self.hidden.HIDDEN_CHAT ? "Hide chat" : "Show chat");
			self.display({which: 'chat', hide: !self.hidden.HIDDEN_CHAT});
		},
		video: () => {
			self.text('hideVideo', self.hidden.HIDDEN_VIDEO ? "Hide video" : "Show video");
			self.display({which: 'video', hide: !self.hidden.HIDDEN_VIDEO});
		},
		attach: (data) => {
			if (data !== self.menu)
				return;

			self.modules.menu.callback('hideVideo', 'click', self.video);
			self.modules.menu.callback('hideChat', 'click', self.chat);
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.started = true;
			self.listen = [
				{id: "EVENT_MENU_ITEMS_CREATE", callback: self.attach}
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
