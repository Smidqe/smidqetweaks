function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'hideOriginals',
			requires: ['menu', 'settings', 'utilities'],
		},
		config: [],
		modules: {},
		menu: [
			{
				id: 'settings',
				format: 'button',
				text: 'Settings'
			},
			{
				id: 'emotes',
				format: 'button',
				text: 'Emotes'
			}
		],
		hide: () => {
			$('#chatControls').addClass('st-controls-hidden');
		},
		unhide: () => {
			$('#chatControls').removeClass('st-controls-hidden');
		},
		callbacks: (data) => {
			if (data !== self.menu)
				return;

			self.modules.menu.callback('settings', 'click', () => window.showConfigMenu());
			self.modules.menu.callback('emotes', 'click', () => Bem.showBerrymoteSearch());
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);

			if (self.modules.settings.get('layout'))
				self.hide();
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.listen = [
				{id: 'EVENT_LAYOUT_ENABLE', callback: self.hide},
				{id: 'EVENT_LAYOUT_DISABLE', callback: self.unhide},
				{id: 'EVENT_MENU_ITEMS_CREATE', callback: self.callbacks}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
