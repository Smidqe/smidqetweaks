function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'layout',
			requires: ['menu', 'toolbar', 'windows', 'settings', 'playlist', 'utilities', 'colors', 'chat'],
		},
		modules: {},
		events: {
			EVENT_LAYOUT_ENABLE: "EVENT_LAYOUT_ENABLE",
			EVENT_LAYOUT_DISABLE: "EVENT_LAYOUT_DISABLE"
		},
		menu: [
			{
				id: 'layout',
				text: 'Windows',
				format: 'menu',
				group: 'windows',
				children: [
					{id: 'rules', text: 'Rules', format: 'button'},
					{id: 'header', text: 'Header', format: 'button'},
					{id: 'footer', text: 'Footer', format: 'button'},
					{id: 'polls', text: 'Polls', format: 'button'},
					{id: 'messages', text: 'Messages', format: 'button'},
					{id: 'login', text: 'Login', format: 'button'},
					{id: 'playlist', text: 'Playlist', format: 'button'},
					{id: 'userlist', text: 'Userlist', format: 'button'},
				]
			},
		],
		windows: [
			{
				id: 'rules',
				selectors: ['#dyn_motd', '#motdwrap'], //1-2 depending if maltweaks has other plans
				wrap: true
			},
			{
				id: 'header',
				selectors: ["#extras, #banner, #countdown-error, #countdown-timers, body > .wrapper:first", "#headwrap"],
				wrap: true,
			},
			{
				id: 'footer',
				wrap: true,
				selectors: ["#dyn_footer", "#main #footwrap"]
			},
			{
				id: 'polls',
				selectors: ['#pollControl, #pollpane'],
				wrap: true,
				classes: ["st-window-overlap"],
			},
			{
				selectors: ["#mailboxDiv"],
				classes: ["st-window-overlap"],
				wrap: true,
				id: 'messages'
			},
			{
				selectors: [".wrapper > #headbar"],
				wrap: true,
				id: 'login'
			},
			{
				id: 'playlist',
				selectors: ['#main #leftpane'],
				wrap: true,
				classes: ['st-window-playlist', 'st-window-overlap']
			},
			{
				selectors: ['#chatlist'],
				classes: ["st-window-overlap", "st-window-users"],
				wrap: true,
				id: 'userlist'
			}
		],
		config: [],
		adapt: (count) => {
			let video = $('#videowrap');
			let drinkwrap = $('#drinkWrap');

			let data = {
				height: count.drinks > 0 ? `calc(100% - ${drinkwrap.height()}px)` : '',
				top: count.drinks > 0 ? drinkwrap.height() : ''
			};

			if (drinkwrap.css('display') !== 'none')
				video.css(data);
		},
		prepare: () => {
			//TODO: instead of different classes, utilise a single st-patch-layout

			$('#chatpane, #videowrap, #drinkWrap, #mailDiv, #mailButtonDiv')
				.add($('.wrapper').last())
				.addClass('st-patch-layout');

			if (!window.PEP)
				self.modules.windows.element('playlist').addClass('st-patch-layout');

			window.scrollBuffersToBottom();

			self.windows.forEach(window => {
				self.modules.menu.callback(window.id, 'click', () => self.modules.windows.display(window.id, {show: true}));
			});

			self.modules.chat.adapt();
			self.modules.toolbar.show('menu');

			$('#st-menu-new-wrap').removeClass('hidden');

			SmidqeTweaks.notify(self.events.EVENT_LAYOUT_ENABLE, {});
		},
		unprepare: () => {
			$('.st-patch-layout').removeClass('st-patch-layout');

			self.modules.chat.adapt();
			self.modules.toolbar.hide('menu');
			$('#st-menu-new-wrap').addClass('hidden');
			SmidqeTweaks.notify(self.events.EVENT_LAYOUT_DISABLE, {});
		},
		enable: () => {
			self.maltweaks = self.modules.settings.get('maltweaks', false);

			SmidqeTweaks.listen(self.listen);

			self.modules.utilities.waitFor({
				type: self.maltweaks ? 'variable' : 'selector',
				checks: self.maltweaks ? ['MT.loaded'] : ['#playlist'],
				callback: self.prepare
			});
		},
		disable: () => {
			self.unprepare();
			SmidqeTweaks.unlisten(self.listen);
		},
		patches: (data) => {
			if (data === 'playlist')
				self.modules.playlist.refresh();

			if (data === 'userlist')
				self.modules.chat.adapt();
		},
		init: () => {
			self.maltweaks = self.modules.settings.get('maltweaks');
			self.listen = [
				{id: self.modules.windows.events.EVENT_WINDOW_OPEN, callback: self.patches},
				{id: 'drinkCount', callback: self.adapt, socket: true},
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
