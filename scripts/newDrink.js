function load() {
	const self = {
		meta: {
			name: 'newDrink',
			group: 'scripts',
			requires: ['colors', 'chat', 'windows'],
		},
		container: null,
		config: [],
		modules: {},
		hide: () => {
			$('#st-toolbar-drinkwrap').addClass('st-window-hidden');
		},
		unhide: () => {
			$('#st-toolbar-drinkwrap').removeClass('st-window-hidden');
		},
		show: (data) => {
			if (data.drinks > 0)
				self.unhide();
			else
				self.hide();

			self.modules.chat.adapt();
			self.update();
		},
		update: () => {
			let count = self.original.find('#drinkCounter').text();
			let dpm = self.original.find('.dpmCounter').text();

			if (dpm === '')
				dpm = '0.00';
			else
				dpm = dpm.slice(6);

			self.container.find('#st-drinkwrap-count > span').text(
				count
			);

			self.container.find('#st-drinkwrap-dpm > span').text(
				dpm
			);
		},
		enable: () => {
			self.container = $('<div>', {id: 'st-toolbar-drinkwrap', class: 'st-toolbar-row'})
				.append(
					$('<div>', {id: 'st-drinkwrap-count', text: 'Drinks: '}).append($('<span>'))
				)
				.append(
					$('<div>', {id: 'st-drinkwrap-dpm', text: 'DPM: '}).append($('<span>'))
				);

			self.container.appendTo('#chatControls');

			self.interval = setInterval(() => {
				self.update();
			}, 1000);

			self.show(false);
			self.original.addClass('st-window-hidden');

			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			self.container.remove();
			self.modules.chat.adapt();

			clearInterval(self.interval);

			SmidqeTweaks.unlisten(self.listen);
			self.original.removeClass('st-window-hidden');
		},
		init: () => {
			self.original = $('#drinkWrap');
			self.listen = [
				{id: 'drinkCount', callback: self.show, socket: true},
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
