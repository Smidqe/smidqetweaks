function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'pollAverage',
			requires: ['settings', 'chat', 'polls']
		},
		name: 'pollAverage',
		config: [{
			title: 'Ignore 0-votes',
			key: 'ignoreZero',
		}],
		polls: null,
		modules: {},
		check: (votes) => {
			if (votes.length < 10 || votes.length > 11)
				return false;

			if (votes.filter(vote => isNaN(parseInt(vote.text))).length > 0)
				return false;

			return true;
		},
		calculate: () => {
			//get the first non active poll
			let poll = self.modules.polls.first(false);

			//where did we go wrong?
			if (poll.ranked || !self.check(poll.options))
				return;

			let zeroIgnored = self.modules.settings.get('ignoreZero');

			if (zeroIgnored && poll.options.length == 11)
				poll.options.shift();

			let total = poll.options.reduce((sum, option) => sum + (parseInt(option.votes) * parseInt(option.text)), 0);
			let votes = poll.options.reduce((sum, option) => sum + parseInt(option.votes), 0);

			//prevent division by 0
			if (votes === 0)
				return;

			let average = total / votes;
			let message = "average is " + average;

			self.modules.chat.add('Episode', message, 'rcv', false);
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.listen = [
				{id: 'clearPoll', callback: self.calculate, socket: true}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
