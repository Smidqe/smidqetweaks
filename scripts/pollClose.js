function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'pollClose',
			requires: ['chat', 'settings', 'polls']
		},
		config: [{
			title: 'Squee upon closure',
			key: 'squeeClose',
		}, {
			title: 'Highlight',
			key: 'highlightClose'
		}],
		polls: null,
		settings: null,
		modules: {},
		notify: () => {
			let poll = self.modules.polls.first(false);
			let message = `${poll.title} was closed`;

			self.modules.chat.add('Poll', message, 'act', false);

			if (self.modules.settings.get('squeeClose'))
				window.doSqueeNotify();

			if (self.modules.settings.get('highlightClose'))
				self.modules.chat.posts({nick: 'Poll'}).last().addClass('highlight');
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.listen = [
				{id: 'clearPoll', callback: self.notify, socket: true}
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
