function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'rcvSquee',
			requires: ['settings', 'chat']
		},
		config: [{
			title: 'Highlight the message',
			key: 'highlightRCV',
		}],
		modules: {},
		notify: (data) => {
			if (data.msg.emote !== 'rcv')
				return;

			window.doSqueeNotify();

			if (self.modules.settings.get('highlightRCV'))
				self.modules.chat.rcv().last().addClass('highlight');
		},
		disable: () => {
			self.modules.chat.unpatch('addChatMsg', self.notify);
		},
		enable: () => {
			self.modules.chat.patch('addChatMsg', self.notify);
		},
	};

	return self;
}
SmidqeTweaks.module("ACTION_MODULE_ADD", load());
