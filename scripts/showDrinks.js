function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'showDrinks',
			requires: ['settings', 'chat']
		},
		config: [],
		chat: null,
		modules: {},
		show: (data) => {
			if (data.msg.emote !== 'drink')
				return;

			let last = self.modules.chat.posts({which: 'drink'}).last();

			//shouldn't trigger, but meh
			if (last.find('.st-chat-drinkcount').length > 0)
				return;

			last.prepend(
				$("<td>", {
					class: 'st-chat-drinkcount',
				}).append($("<span>", {
					text: 'x' + $('#drinkCounter').text(),
				}))
			);
		},
		disable: () => {
			self.modules.chat.unpatch('addChatMsg', self.show);
		},
		enable: () => {
			self.modules.chat.patch('addChatMsg', self.show);
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
