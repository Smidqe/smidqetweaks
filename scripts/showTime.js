function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'showTime',
			requires: ['time', 'toolbar', 'settings'],
			subgroup: 'time'
		},
		config: [{
			title: '12H format',
			key: '12hour',
		}, {
			title: 'Seconds',
			key: 'showSeconds',
		}],
		toolbar: [{
			id: 'time',
			element: 'span',
			tooltip: 'Current time'
		}],
		modules: {},
		update: () => {
			let time = self.modules.time.get();
			let half = self.modules.settings.get('12hour', false);

			if (half)
				time = self.modules.time.convert(time, '12h');

			let text = '';
			let skip = ['format', 'suffix'];

			$.each(time, (key, val) => {
				if (skip.indexOf(key) !== -1)
					return;

				if (key === 's' && !self.modules.settings.get('showSeconds', false))
					return;

				if (key !== 'h')
					text += ':';

				text += val;
			});

			if (half)
				text += " " + time.suffix;

			self.modules.toolbar.get('time').text(text);
		},
		restart: (data) => {
			if (data.key !== 'showSeconds' && data.key !== 'start')
				return;

			clearInterval(self.updater);

			self.updater = setInterval(
				self.update,
				data.value ? 1000 : 60000
			);

			self.update();
		},
		enable: () => {
			self.restart({key: 'start', value: self.modules.settings.get('showSeconds', false)});
			SmidqeTweaks.listen({id: 'EVENT_SETTING_CHANGE', callback: self.restart});
		},
		disable: () => {
			clearInterval(self.updater);
			SmidqeTweaks.unlisten({id: 'EVENT_SETTING_CHANGE', callback: self.restart});
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
