/*
	TODO:
		Enable functionality in the menu.js
		Grabber function
*/

function load() {
	const self = {
		meta: {
			name: 'themeSelector',
			group: 'scripts',
			requires: ['settings', 'menu']
		},
		config: [],
		menu: [],
		themes: [],
		grab: () => {
			return window.groupList.map((grp) => {
				let themes = grp.themeButtons.map(btn => {
					return {
						name: btn.themeName,
						path: btn.themeUrl,
					};
				});

				return {
					grp: grp.groupName,
					themes: themes,
				};
			});
		},
		enable: () => {

		},
		disable: () => {

		},
		init: () => {
			self.themes = self.grab();
		}
	};

	return self;
}

SmidqeTweaks.add(load());
