/*
    TODO:
        -
*/

function load() {
	const self = {
		meta: {
			group: 'scripts',
			name: 'titleWrap',
			requires: ['settings', 'chat', 'toolbar', 'utilities']
		},
		config: [],
		modules: {},
		patch: () => {
			self.modules.chat.adapt();
		},
		enable: () => {
			if (!self.modules.settings.berrytweaks('videoTitle'))
				return self.modules.settings.set('titleWrap', false, true);

			self.modules.utilities.waitFor({
				type: 'element',
				checks: ['#chatControls', '#berrytweaks-video_title'],
				callback: () => {
					self.container
						.appendTo("#chatControls")
						.append(
							$('#berrytweaks-video_title')
						);
				}
			});

			SmidqeTweaks.listen(self.listen);
		},
		disable: () => {
			SmidqeTweaks.unlisten(self.listen);

			self.container.contents().unwrap();
		},
		init: () => {
			self.container = $('<div>', {id: 'st-videotitle-window', class: 'st-toolbar-row'});

			self.listen = [
				{id: 'forceVideoChange', callback: self.patch, socket: true},
				{id: 'hbVideoDetail', callback: self.patch, socket: true}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
