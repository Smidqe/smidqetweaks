function load() {
	const self = {
		config: [{
			title: 'Show current position',
			key: 'trackCurrent',
		}],
		meta: {
			group: 'scripts',
			name: 'trackPlaylist',
			requires: ['playlist', 'chat', 'settings'],
		},
		tracking: {},
		modules: {},
		patch: ['remove', 'insertAfter', 'append', 'insertBefore'],
		started: false,
		socket: [],
		changes: {
			CHANGE_NONE: -1,
			CHANGE_VOLATILE: 0,
			CHANGE_POSITION: 1,
			CHANGE_ADD: 2,
			CHANGE_REMOVE: 3,
		},
		track: (video) => {
			let title = decodeURIComponent(video.videotitle);
			let position = self.modules.playlist.get('title', title).pos;

			return self.tracking[video.videoid] = {
				...video,
				title: title,
				pos: position,
				timeout: 0,
				change: {
					id: self.changes.CHANGE_NONE,
					old: null,
					new: null,
				},
			};
		},
		message: (data) => {
			let initial = `${data.title}${data.volat ? ' (volatile)' : ''}`;
			let command = null;

			switch (data.change.id) {
				case self.changes.CHANGE_VOLATILE: {
					command = ` volatility was changed from ${data.change.old} to ${data.change.new}`;
					break;
				}
				case self.changes.CHANGE_POSITION: {
					command = ` position was changed from ${data.change.old} to ${data.change.new}`;
					break;
				}
				case self.changes.CHANGE_ADD: {
					command = ` was added to playlist`;
					break;
				}
				case self.changes.CHANGE_REMOVE: {
					command = ` was removed from playlist`;
					break;
				}
			}

			let message = `${initial}${command}`;

			if (self.modules.settings.get('trackCurrent') && data.change.id === self.changes.CHANGE_POSITION)
				message = `${initial}${command}, current: ${self.modules.playlist.position('title', decodeURIComponent(window.ACTIVE.videotitle))}`;

			/*
				TODO:
					- Readd the trackCurrent setting here
					- Rework the messages a bit to make them more clear
			*/

			self.modules.chat.add('Playlist', `${message}`, 'act', true);
		},
		change: (obj, id, old, val) => {
			if (old === val)
				return;

			obj.change = {
				id,
				old,
				new: val
			};

			switch (id) {
				case self.changes.CHANGE_POSITION: obj.pos = val; break;
				case self.changes.CHANGE_VOLATILE: obj.volat = val; break;
			}
		},
		action: (data) => {
			if (!self.started || !data)
				return;

			let object = self.tracking[data.videoid];
			let volatile = self.modules.playlist.get('index', data.pos).value;

			if (!object)
				object = self.track(volatile || data);

			object.change.id = data.action;

			//console.log(data);

			switch (data.action) {
				case self.changes.CHANGE_ADD: object.change.id = self.changes.CHANGE_ADD; break;
				case self.changes.CHANGE_REMOVE: {
					object.timeout = setTimeout(() => {
						if (!self.modules.playlist.exists('title', object.title))
							self.message(object);

						delete self.tracking[object.videoid];
					}, 1000);

					break;
				}
				case self.changes.CHANGE_POSITION: {
					clearTimeout(object.timeout);

					let position = self.modules.playlist.get('title', object.title).pos;

					if (object.pos === position)
						object.change.id = self.changes.CHANGE_NONE;
					else
						self.change(object, data.action, object.pos, position);

					break;
				}
				case self.changes.CHANGE_VOLATILE: {
					self.change(object, data.action, !data.volat, data.volat);
					break;
				}
			}

			if ([self.changes.CHANGE_NONE, self.changes.CHANGE_REMOVE].indexOf(object.change.id) === -1) {
				self.message(object);
				object.change.id = self.changes.CHANGE_NONE;
			}
		},
		enable: () => {
			window.waitForFlag('PLREADY', () => {
				self.patch.forEach(value => {
					self.modules.playlist.patch(value, self.proto, value !== 'remove');
				});

				SmidqeTweaks.listen(self.socket);
				self.started = true;
			});
		},
		disable: () => {
			self.patch.forEach(value => {
				self.modules.playlist.unpatch(value, self.proto);
			});

			SmidqeTweaks.unlisten(self.socket);
		},
		proto: (node, newNode) => {
			let action = self.changes.CHANGE_POSITION;

			if (!newNode)
				action = self.changes.CHANGE_REMOVE;

			self.action({...(newNode || node), action});
		},
		init: () => {
			self.socket = [
				{id: 'addVideo', callback: (data) => self.action({...data.video, action: self.changes.CHANGE_ADD}), socket: true},
				{id: 'setVidVolatile', callback: (data) => self.action({...data, action: self.changes.CHANGE_VOLATILE}), socket: true}
			];
		},
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
