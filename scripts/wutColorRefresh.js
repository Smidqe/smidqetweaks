function load() {
	const self = {
		meta: {
			name: 'wutColorRefresh',
			group: 'scripts',
			requires: ['utilities', 'windows', 'settings']
		},
		config: [],
		original: null,
		moved: false,
		modules: {},
		move: () => {
			if (self.moved)
				return;

			self.modules.utilities.waitFor({
				type: 'element',
				checks: ['#wutColorRefresh', '#st-window-container-userlist'],
				callback: () => {
					self.modules.windows
						.element('userlist')
						.find('.st-titlebar:first')
						.append(
							$('#wutColorRefresh')
						);

					self.moved = true;
				}
			});
		},
		remove: () => {
			self.original.append($('#wutColorRefresh'));
		},
		enable: () => {
			SmidqeTweaks.listen(self.listen);

			if (self.modules.settings.get('layout'))
				self.move();
		},
		disable: () => {
			if ($('#wutColorRefresh').parent().attr('id') !== 'chatlist')
				self.remove();

			SmidqeTweaks.unlisten(self.listen);
		},
		init: () => {
			self.original = $('#chatlist');
			self.listen = [
				{id: 'EVENT_LAYOUT_ENABLE', callback: self.move},
				{id: 'EVENT_LAYOUT_DISABLE', callback: self.remove}
			];
		}
	};

	return self;
}

SmidqeTweaks.module("ACTION_MODULE_ADD", load());
