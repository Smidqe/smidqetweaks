// ==UserScript==
// @author      Smidqe
// @name        SmidqeTweaks 2
// @namespace   SmidqeTweaks
// @description SmidqeTweaks 2 for BT
// @include     https://berrytube.tv/*
// @include		
// @version     1.3
// @grant       none
// ==/UserScript==


(function() {
    'use strict';

    var script = document.createElement("script");
    script.setAttribute('src', 'https://smidqe.gitlab.io/smidqetweaks/stweaks.js');
    document.head.appendChild(script);


})();
