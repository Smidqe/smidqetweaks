const self = {
	constants: {
		ACTION_MODULE_LOAD: "ACTION_MODULE_LOAD",
		ACTION_MODULE_UNLOAD: "ACTION_MODULE_UNLOAD",
		ACTION_MODULE_ADD: "ACTION_MODULE_ADD",
		ACTION_MODULE_LOAD_REQS: "ACTION_MODULE_LOAD_REQS",
		ACTION_MODULE_START: "ACTION_MODULE_START",
		ACTION_MODULE_STOP: "ACTION_MODULE_STOP",

		EVENT_MODULE_LOAD: "EVENT_MODULE_LOAD",
		EVENT_MODULE_UNLOAD: "EVENT_MODULE_UNLOAD",
		EVENT_MODULE_STOP: "EVENT_MODULE_STOP",
		EVENT_MODULE_START: "EVENT_MODULE_START",
	},
	modules: {},
	callbacks: {},
	dependencies: {},
	queue: [],
	events: {
		EVENT_MODULE_LOAD: [],
		EVENT_MODULE_UNLOAD: []
	},
	socket: [],
	names: {
		modules: [
			'settings', 'toolbar', 'windows', 'playlist',
			'menu', 'time', 'chat', 'video',
			'utilities', 'polls', 'colors'
		],
		scripts: [
			'layout', 'pollAverage', 'rcvSquee', 'titleWrap',
			'showDrinks', 'trackPlaylist', 'showTime', 'berryControls',
			'wutColorRefresh', 'pollClose', 'announceBerry',
			'newDrink', 'fullScreen', 'hideOriginals',
		],
		thirdparty: [
			'maltweaks', 'wutcolors', 'berrytweaks'
		]
	},
	groups: {
		others: ['berrytweaks', 'maltweaks', 'wutcolors'],
		layout: ['layout', 'newDrink', 'hideOriginals'],
		time: ['showTime'],
		chat: ['rcvSquee', 'showDrinks'],
		playlist: ['trackPlaylist'],
		polls: ['pollClose', 'pollAverage'],
		berry: ['berryControls', 'announceBerry'],
		patches: ['titleWrap', 'wutColorRefresh'],
		random: ['fullScreen'],
	},
	descriptions: {
		layout: 'Use custom layout',
		pollClose: 'Notify when poll closes',
		pollAverage: 'Calculate episode average',
		rcvSquee: 'Notify on RCV messages',
		titleWrap: 'Fix BerryTweaks videotitle wrap',
		showDrinks: 'Show the amount of drinks in chat',
		trackPlaylist: 'Track playlist changes',
		showTime: 'Show time in toolbar',
		berryControls: 'Modularize playlist/poll controls on berry',
		hideOriginals: 'Hide the original emote/settings buttons',
		wutColorRefresh: 'Move wutColors refresh button to titlebar',
		announceBerry: 'Announce when berry is given',
		newDrink: 'Alternative drink count/dpm element',
		fullScreen: 'Hide video or chat',
		maltweaks: 'Maltweaks',
		wutcolors: 'wutColors',
		berrytweaks: 'BerryTweaks',
	},
	unpatch: (data) => {
		if (data instanceof Array)
			return data.map(sub => self.unpatch(sub));

		self.callbacks[data.name][data.func] = self.callbacks[data.name][data.func].filter((value) =>
			data.callback !== value
		);
	},
	patch: (data) => {
		if (data instanceof Array)
			return data.map(sub => self.patch(sub));

		if (!self.callbacks[data.name])
			self.callbacks[data.name] = {};

		let callbacks = self.callbacks[data.name];
		let original = data.container[data.func];

		if (!callbacks[data.func])
			callbacks[data.func] = [original];

		if (data.after)
			callbacks[data.func].push(data.callback);
		else
			callbacks[data.func].unshift(data.callback);

		data.container[data.func] = function() {
			let result;

			for (let f of self.callbacks[data.name][data.func])
			{
				try {
					if (f === original)
						result = f.apply(this, arguments);
					else
						f.apply(this, arguments);
				} catch (error) {
					console.log(error);
				}
			}

			return result;
		};
	},
	listen: (data) => {
		if (data instanceof Array)
			return data.map(sub => self.listen(sub));

		if (data.socket) {
			if (!self.socket.has(data.id))
				return;

			socket.on(data.id, data.callback);
		} else {
			if (!self.events[data.id])
				self.events[data.id] = [];

			self.events[data.id].push(data.callback);
		}
	},
	unlisten: (data) => {
		if (data instanceof Array)
			return data.map(sub => self.unlisten(sub));

		if (data.socket)
			return socket.removeListener(data.id, data.callback);

		self.events[data.id] = self.events[data.id].filter(func => func !== data.callback);
	},
	notify: (id, data) => {
		console.log('ST_NOTIFY:', id, data);
		self.events[id].map(func => func(data));
	},
	module: (action, data) => {
		switch (action) {
			case "ACTION_MODULE_LOAD": {
				if (self.modules[data.name] || (self.queue.indexOf(data.name) !== -1))
					return;

				$.ajax(data.path || `https://smidqe.gitlab.io/smidqetweaks/${data.dir}/${data.name}.js`, {
					cache: false,
					dataType: "script",
				});

				self.queue.push(data.name);
				break;
			}
			case "ACTION_MODULE_ADD": {
				self.modules[data.meta.name] = data;

				Object.assign(self.constants, data.events);

				for (let event in data.events || [])
					self.events[event] = self.events[event] || [];

				for (let mod of data.meta.requires || [])
					self.dependencies[mod] += 1;

				self.module("ACTION_MODULE_START", data);
				break;
			}
			case "ACTION_MODULE_LOAD_REQS": {
				let start = true;

				data.forEach((mod) => {
					if (!start)
						return;

					if ((self.queue.indexOf(mod) === -1) && !self.modules[mod])
						self.module("ACTION_MODULE_LOAD", {dir: 'modules', name: mod});

					start = self.modules[mod] ? self.modules[mod].started : false;
				});

				return start;
			}
			case "ACTION_MODULE_START": {
				let interval = setInterval(() => {
					if (self.module("ACTION_MODULE_LOAD_REQS", data.meta.requires || [])) {
						for (let sub of data.meta.requires || []) {
							data.modules[sub] = self.modules[sub];
						}

						if (data.init)
							data.init();

						if (data.enable)
							data.enable();

						self.notify(self.constants.EVENT_MODULE_LOAD, data);
						self.queue.splice(self.queue.indexOf(data.meta.name), 1);

						clearInterval(interval);
					}
				}, 250);
				break;
			}
			case "ACTION_MODULE_UNLOAD": {
				let mod = self.modules[data.name];

				if (mod.disable)
					mod.disable();

				(mod.meta.requires || []).forEach((req) => {
					let amount = (self.dependencies[req] -= 1);

					if (amount == 0)
						self.module("ACTION_MODULE_UNLOAD", {name: req});
				});

				Object.keys(mod.events || {}).forEach((ev) => {
					delete self.constants[ev];
					delete self.events[ev];
				});

				self.notify("EVENT_MODULE_UNLOAD", mod);

				delete self.modules[data.name];

				break;
			}
		}
	},
	init: () => {
		self.socket = new Set();
		console.log("Loading Smidqetweaks");

		$('head').append(
			$('<link id="st-stylesheet" rel="stylesheet" type="text/css" href="https://smidqe.gitlab.io/smidqetweaks/css/stweaks.css"/>'),
			$('<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">')
		);

		Object.keys(window.socket.$events).forEach((event) => {
			self.socket.add(event);
		});

		self.module("ACTION_MODULE_LOAD", {dir: 'modules', name: 'settings'});
		self.dependencies.settings = 1;
	},
};

window.SmidqeTweaks = self;
self.init();
